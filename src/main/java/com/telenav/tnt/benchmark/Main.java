/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.telenav.tnt.benchmark;

import com.telenav.tnt.benchmark.data.Gps;
import com.telenav.tnt.benchmark.data.GpsList;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Random;

public class Main {

    private static SerializationBenchmarkItem[] items = new SerializationBenchmarkItem[]{
//            new SerializationBenchmarkItem("12, 20000 times", 12, 20000),
            new SerializationBenchmarkItem("Integer.MAX_VALUE, 20000 times", Integer.MAX_VALUE, 20000),
//            new SerializationBenchmarkItem("4000l, 20000 times", 4000l, 20000),
//            new SerializationBenchmarkItem("Long.MAX_VALUE, 20000 times", Long.MAX_VALUE, 20000),
//            new SerializationBenchmarkItem("123.123, 20000 times", 123.123, 20000),
//            new SerializationBenchmarkItem("Double.MAX_VALUE, 20000 times", Double.MAX_VALUE, 20000),
//            new SerializationBenchmarkItem("Boolean.TRUE, 20000 times", Boolean.TRUE, 20000),
//
//            new SerializationBenchmarkItem("'This is a serialization benchmark!', 20000 times", "This is a serialization benchmark!", 20000),
//
//            new SerializationBenchmarkItem("GPS Object, 2000 times", createGps(), 2000),
//            new SerializationBenchmarkItem("100 Gps list, 2000 times", createGpsList(100), 2000),
//            new SerializationBenchmarkItem("100 duplicated Gps list, 2000 times", createDuplicatedGpsList(100), 2000)
    };

    public static void main(String[] args) throws UnsupportException, IOException {
        for (SerializationBenchmarkItem item : items) {
            System.out.println(item.getName() + ":");
            new SerializationBenchmarkRunner(item.getData(), item.getTimes()).run();
        }
    }

    private static GpsList createDuplicatedGpsList(int count) {
        GpsList gpsList = new GpsList();
        Gps gps = createGps();

        gpsList.setGpses(new Gps[count]);
        for (int i = 0; i < count; i++) {
            gpsList.getGpses()[i] = gps;
        }

        return gpsList;
    }

    private static GpsList createGpsList(int count) {
        GpsList gpsList = new GpsList();

        gpsList.setGpses(new Gps[count]);
        for (int i = 0; i < count; i++) {
            gpsList.getGpses()[i] = createGps();
        }

        return gpsList;
    }

    private static Gps createGps() {
        Random random = new Random();

        Gps gps = new Gps();
        gps.setLat(random.nextDouble() * 90);
        gps.setLon(random.nextDouble() * 180);
        gps.setGpsTime(System.currentTimeMillis());
        gps.setCreateTime(System.currentTimeMillis());
        gps.setHeading(random.nextDouble() * 360);
        gps.setAltitude(random.nextDouble() * 120);
        gps.setVelocity(random.nextDouble() * 70);

        return gps;
    }

    static Logger logger = Logger.getLogger(Main.class);
}
