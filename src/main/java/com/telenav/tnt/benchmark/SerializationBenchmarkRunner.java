package com.telenav.tnt.benchmark;

import com.telenav.tnt.benchmark.serialization.*;

public class SerializationBenchmarkRunner {

    private Serializer[] serializers = new Serializer[] {
            new JavaNativeSerializer(),
            new HessianSerializer(),
            new Hessian2Serializer(),
            new GsonSerializer(),
            new FastJsonSerializer(),
            new MsgpackSerializer(),
            new JaxbSerializer()
    };
    
    private Object data;
    private int times;

    public SerializationBenchmarkRunner(Object data, int times) {
        this.data = data;
        this.times = times;
    }

    public void run() {
        for(Serializer serializer:serializers) {
            SerializationBenchmarker benchmarker =
                    new SerializationBenchmarker(serializer, data, times);
            
            try {
                BenchmarkResult benchmarkResult = benchmarker.benchmark();

                System.out.println(benchmarkResult);
            } catch (UnsupportException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
