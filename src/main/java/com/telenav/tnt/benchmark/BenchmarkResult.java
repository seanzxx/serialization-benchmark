package com.telenav.tnt.benchmark;


public class BenchmarkResult {
    private String name;
    private long serializeTime;
    private long deserializeTime;
    private long roundTime;
    private long space;

    public BenchmarkResult(String name, long serializeTime, long deserializeTime, long roundTime, long space) {
        this.name = name;
        this.serializeTime = serializeTime;
        this.deserializeTime = deserializeTime;
        this.roundTime = roundTime;
        this.space = space;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSerializeTime() {
        return serializeTime;
    }

    public void setSerializeTime(long serializeTime) {
        this.serializeTime = serializeTime;
    }

    public long getDeserializeTime() {
        return deserializeTime;
    }

    public void setDeserializeTime(long deserializeTime) {
        this.deserializeTime = deserializeTime;
    }

    public long getRoundTime() {
        return roundTime;
    }

    public void setRoundTime(long roundTime) {
        this.roundTime = roundTime;
    }

    public long getSpace() {
        return space;
    }

    public void setSpace(long space) {
        this.space = space;
    }

    @Override
    public String toString() {
        return  name+"\t"+serializeTime+"\t"+deserializeTime+"\t"+space;
    }
}
