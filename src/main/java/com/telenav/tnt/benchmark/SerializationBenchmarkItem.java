package com.telenav.tnt.benchmark;


public class SerializationBenchmarkItem {
    private Object data;
    private String name;
    private int times;

    public SerializationBenchmarkItem(String name, Object data, int times) {
        this.data = data;
        this.name = name;
        this.times = times;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTimes() {
        return times;
    }

    public void setTimes(int times) {
        this.times = times;
    }
}
