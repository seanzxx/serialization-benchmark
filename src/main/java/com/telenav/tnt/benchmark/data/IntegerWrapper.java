package com.telenav.tnt.benchmark.data;


import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
public class IntegerWrapper implements Serializable {
    int value;

    public IntegerWrapper() {
    }

    public IntegerWrapper(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}


