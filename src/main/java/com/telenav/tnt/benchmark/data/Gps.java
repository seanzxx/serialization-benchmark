package com.telenav.tnt.benchmark.data;

import org.msgpack.annotation.Message;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@Message
@XmlRootElement
public class Gps implements Serializable {
    private long gpsTime = 0L;
    private double lat = 0.0D;
    private double lon = 0.0D;
    private double velocity = 0.0D;
    private double heading = 0.0D;
    private double altitude = 0.0D;
    private double hdop = 0.0D;
    private double pdop = 0.0D;

    private long createTime = 0L;
    private byte reserve0 = 0;
    private byte reserve1 = 0;
    private byte reserve2 = 0;
    private byte reserve3 = 0;

    public long getGpsTime() {
        return gpsTime;
    }

    public void setGpsTime(long gpsTime) {
        this.gpsTime = gpsTime;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getVelocity() {
        return velocity;
    }

    public void setVelocity(double velocity) {
        this.velocity = velocity;
    }

    public double getHeading() {
        return heading;
    }

    public void setHeading(double heading) {
        this.heading = heading;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double getHdop() {
        return hdop;
    }

    public void setHdop(double hdop) {
        this.hdop = hdop;
    }

    public double getPdop() {
        return pdop;
    }

    public void setPdop(double pdop) {
        this.pdop = pdop;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public byte getReserve0() {
        return reserve0;
    }

    public void setReserve0(byte reserve0) {
        this.reserve0 = reserve0;
    }

    public byte getReserve1() {
        return reserve1;
    }

    public void setReserve1(byte reserve1) {
        this.reserve1 = reserve1;
    }

    public byte getReserve2() {
        return reserve2;
    }

    public void setReserve2(byte reserve2) {
        this.reserve2 = reserve2;
    }

    public byte getReserve3() {
        return reserve3;
    }

    public void setReserve3(byte reserve3) {
        this.reserve3 = reserve3;
    }

    @Override
    public String toString() {
        return "Gps{" +
                "gpsTime=" + gpsTime +
                ", lat=" + lat +
                ", lon=" + lon +
                ", velocity=" + velocity +
                ", heading=" + heading +
                ", altitude=" + altitude +
                ", hdop=" + hdop +
                ", pdop=" + pdop +
                ", createTime=" + createTime +
                ", reserve0=" + reserve0 +
                ", reserve1=" + reserve1 +
                ", reserve2=" + reserve2 +
                ", reserve3=" + reserve3 +
                '}';
    }
}
