package com.telenav.tnt.benchmark.data;


import org.msgpack.annotation.Message;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Message
@XmlRootElement
public class GpsList implements Serializable {
    private Gps[] gpses;

    public Gps[] getGpses() {
        return gpses;
    }

    public void setGpses(Gps[] gpses) {
        this.gpses = gpses;
    }
}
