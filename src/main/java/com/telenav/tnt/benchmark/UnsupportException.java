package com.telenav.tnt.benchmark;


public class UnsupportException extends Exception{
    
    public UnsupportException(String name, Object data, Exception cause) {
        super(String.format("%s doesn't support type %s", name, data.getClass().getName()), cause);
    }
}
