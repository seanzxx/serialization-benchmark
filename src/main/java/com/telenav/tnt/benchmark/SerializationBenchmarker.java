package com.telenav.tnt.benchmark;


import com.telenav.tnt.benchmark.serialization.Serializer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public class SerializationBenchmarker implements Benchmarker {
    private Serializer serializer;
    private Object data;
    private int times;

    public SerializationBenchmarker(Serializer serializer, Object data, int times) {
        this.serializer = serializer;
        this.data = data;
        this.times = times;
    }

    public BenchmarkResult benchmark() throws UnsupportException {
        long start = 0;
        long end = 0;
        byte[] bytes = new byte[0];

        try {
            start = System.currentTimeMillis();
            for (int i = 0; i < times; i++) {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();

                serializer.serialize(data, bos);

                if (i == 0) {
                    bytes = bos.toByteArray();
                }
            }
            end = System.currentTimeMillis();
            long serializeTime = end - start;

            start = System.currentTimeMillis();
            for (int i = 0; i < times; i++) {
                ByteArrayInputStream bis = new ByteArrayInputStream(bytes);

                Object result = serializer.deserialize(data.getClass(), bis);
            }
            end = System.currentTimeMillis();
            long deserializeTime = end - start;

            return new BenchmarkResult(
                    serializer.getName(),
                    serializeTime,
                    deserializeTime,
                    serializeTime + deserializeTime,
                    bytes.length);
        } catch (Exception e) {
            throw new UnsupportException(serializer.getName(), data, e);
        }
    }
}
