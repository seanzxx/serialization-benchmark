package com.telenav.tnt.benchmark;


public interface Benchmarker {
    BenchmarkResult benchmark() throws UnsupportException;
}
