package com.telenav.tnt.benchmark.serialization;

import com.caucho.hessian.io.HessianInput;
import com.caucho.hessian.io.HessianOutput;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class HessianSerializer implements Serializer {

    public String getName() {
        return "Hessian";
    }

    public void serialize(Object data, OutputStream outputStream) throws IOException {
        HessianOutput hessionOutput = new HessianOutput(outputStream);

        hessionOutput.writeObject(data);
        hessionOutput.close();
    }

    public Object deserialize(Class clazz, InputStream inputStream) throws IOException {
        HessianInput hession2Input = new HessianInput(inputStream);

        Object result = hession2Input.readObject();
        hession2Input.close();
        return result;
    }
}
