package com.telenav.tnt.benchmark.serialization;

import java.io.InputStream;
import java.io.OutputStream;

public interface Serializer {
    public String getName();
    
    public void serialize(Object data, OutputStream outputStream) throws Exception;
    public Object deserialize(Class clazz, InputStream inputStream) throws Exception;
}
