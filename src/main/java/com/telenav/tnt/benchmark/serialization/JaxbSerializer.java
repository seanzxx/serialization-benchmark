package com.telenav.tnt.benchmark.serialization;


import com.telenav.tnt.benchmark.data.Gps;
import com.telenav.tnt.benchmark.data.GpsList;
import com.telenav.tnt.benchmark.data.IntegerWrapper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;
import java.io.OutputStream;

public class JaxbSerializer implements Serializer {

    private Marshaller marshaler;
    private Unmarshaller unmarshaller;

    public JaxbSerializer() {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Gps.class, GpsList.class, IntegerWrapper.class);
            marshaler = jaxbContext.createMarshaller();
            unmarshaller = jaxbContext.createUnmarshaller();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public String getName() {
        return "JAXB";
    }

    public void serialize(Object data, OutputStream outputStream) throws Exception {
        if (marshaler == null) {
            throw new IllegalStateException("JAXB cannot be found!");
        }

        marshaler.marshal(data, outputStream);

    }

    public Object deserialize(Class clazz, InputStream inputStream) throws Exception {
        if (unmarshaller == null) {
            throw new IllegalStateException("JAXB cannot be found!");
        }

        return unmarshaller.unmarshal(inputStream);
    }
}
