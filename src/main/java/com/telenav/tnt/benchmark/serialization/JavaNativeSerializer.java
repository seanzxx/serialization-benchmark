package com.telenav.tnt.benchmark.serialization;

import java.io.*;

public class JavaNativeSerializer implements Serializer {

    public String getName() {
        return "Java Native";
    }

    public void serialize(Object data, OutputStream outputStream) throws IOException {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);

        objectOutputStream.writeObject(data);
        objectOutputStream.close();
    }

    public Object deserialize(Class clazz, InputStream inputStream) throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);

        Object result = objectInputStream.readObject();
        objectInputStream.close();
        return result;
    }
}
