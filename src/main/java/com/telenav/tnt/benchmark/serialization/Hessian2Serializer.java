package com.telenav.tnt.benchmark.serialization;

import com.caucho.hessian.io.Hessian2Input;
import com.caucho.hessian.io.Hessian2Output;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Hessian2Serializer implements Serializer {

    public String getName() {
        return "Hessian2";
    }

    public void serialize(Object data, OutputStream outputStream) throws IOException {
        Hessian2Output hession2Output = new Hessian2Output(outputStream);

        hession2Output.writeObject(data);
        hession2Output.close();
    }

    public Object deserialize(Class clazz, InputStream inputStream) throws IOException {
        Hessian2Input hession2Input = new Hessian2Input(inputStream);
        
        Object result = hession2Input.readObject();
        hession2Input.close();
        return result;
    }
}
