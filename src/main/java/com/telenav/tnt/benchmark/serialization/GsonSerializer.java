package com.telenav.tnt.benchmark.serialization;


import com.google.gson.Gson;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class GsonSerializer implements Serializer {

    private final Gson gson = new Gson();

    public String getName() {
        return "Gson";
    }

    public void serialize(Object data, OutputStream outputStream) throws Exception {
        OutputStreamWriter osw = new OutputStreamWriter(outputStream, "UTF-8");

        gson.toJson(data, osw);
        osw.flush();
    }

    public Object deserialize(Class clazz, InputStream inputStream) throws Exception {
        InputStreamReader isr = new InputStreamReader(inputStream, "utf8");

        Object object = gson.fromJson(isr, clazz);
        return object;
    }
}
