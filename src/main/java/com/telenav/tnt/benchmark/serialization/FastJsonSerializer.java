package com.telenav.tnt.benchmark.serialization;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;

import java.io.InputStream;
import java.io.OutputStream;

public class FastJsonSerializer implements Serializer {

    public String getName() {
        return "FastJson";
    }

    public void serialize(Object data, OutputStream outputStream) throws Exception {
        byte[] bytes = JSON.toJSONBytes(data, SerializerFeature.DisableCircularReferenceDetect);

        outputStream.write(bytes);
    }

    public Object deserialize(Class clazz, InputStream inputStream) throws Exception {
        byte[] bytes = new byte[inputStream.available()];
        inputStream.read(bytes);

        Object result = JSON.parseObject(bytes, clazz);

        return result;
    }
}
